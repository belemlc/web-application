# Instalar Laravel com Doker

- Clonar o laravel do git 
    - git clone https://github.com/laravel/laravel.git sistema
- Entrar na pasta sistema e alterar o arquivo .env.example para .env
- Instalar o composer for windows 
    - https://getcomposer.org/Composer-Setup.exe
- Rodar o composer dentro da pasta sistema
    - composer install
- Gerar uma app key, entrando dentro da pasta sistema, rodar o seguinte comando:
    - php artisan key:generate
- Reconstruir novamente a imagem
    - docker-compose down
    - docker-compose up -d --build